
//What directive is used by Node.js in loading the modules it needs?
//* v8
//What Node.js module contains a method for server creation?
//* HTTP module
//What is the method of the http object responsible for creating a server using Node.js?
//* createServer()
//What method of the response object allows us to set status codes and content types?
//* response.writeHead
//Where will console.log() output its contents when run in Node.js?
//* localhost? terminal?
//What property of the request object contains the address' endpoint?
//* request.url


//console.log("Server is running Sucessfully")
const HTTP = require('http');

//Creates an HTTP server using 
//createServer(requestListener()) method
HTTP.createServer((request,response) => {

    if(request.url === "/register"){
        response.writeHead(300,{"Content-Type":"text/plain"});
        response.write("I'm sorry the page you are looking for cannot be found.");
        response.end()
    } else if (request.url === "/login"){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.write("Welcome to the login page");
        response.end()
    }else {
        response.writeHead(400,{"Content-Type":"text/plain"});
        response.write("Error");
        response.end()
    
    } 

}).listen(4000)